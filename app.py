import os
from flask import Flask

app = Flask(__name__, static_url_path='/')


@app.route('/api/test')
def test() -> str:
    return 'Hello from Flask!'


if __name__ == '__main__':
    HOST = os.environ.get('SERVER_HOST', '0.0.0.0')
    try:
        PORT = int(os.environ.get('PORT', '8080'))
    except ValueError:
        PORT = 8080
    app.run(HOST, PORT)
